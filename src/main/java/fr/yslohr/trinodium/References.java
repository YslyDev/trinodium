package fr.yslohr.trinodium;

public class References {
	public static final String MOD_ID = "trinodium";
	public static final String MOD_NAME = "Trinodium";
	public static final String MOD_VERSION = "1.0";
	public static final String CLIENT_PROXY = "fr.yslohr.trinodium.proxy.ClientProxy";
	public static final String COMMON_PROXY = "fr.yslohr.trinodium.proxy.CommonProxy";
}
