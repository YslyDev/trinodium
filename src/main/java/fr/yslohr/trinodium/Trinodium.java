package fr.yslohr.trinodium;



import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.Mod.EventHandler;
import cpw.mods.fml.common.Mod.Instance;
import cpw.mods.fml.common.SidedProxy;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.event.FMLPostInitializationEvent;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;
import cpw.mods.fml.common.registry.GameRegistry;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import fr.yslohr.trinodium.init.BlocksRegistry;
import fr.yslohr.trinodium.init.ItemsRegistry;
import fr.yslohr.trinodium.init.OreDictionaryRegistry;
import fr.yslohr.trinodium.proxy.ClientProxy;
import fr.yslohr.trinodium.proxy.CommonProxy;
import fr.yslohr.trinodium.world.OreGeneration;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;

@Mod(modid = References.MOD_ID, name = References.MOD_NAME, version = References.MOD_VERSION)
public class Trinodium {
	@SidedProxy(clientSide = References.CLIENT_PROXY, serverSide = References.COMMON_PROXY)
	public static CommonProxy comProxy;
	public static ClientProxy clProxy;
	@Instance(References.MOD_ID)
	public static Trinodium instance;
	
	//Trinodium Creative Tab
	public static CreativeTabs tabTrinodium = new CreativeTabs("tabTrinodium") {
		@SideOnly(Side.CLIENT)
		public Item getTabIconItem() {
			return ItemsRegistry.trinodium_ingot;
		}
	};
	@EventHandler
	public void preInit(FMLPreInitializationEvent event) {

		ItemsRegistry.init();
		ItemsRegistry.register();
		BlocksRegistry.init();
		BlocksRegistry.register();
		OreDictionaryRegistry.register();
		OreDictionaryRegistry.crafting();
		GameRegistry.registerWorldGenerator(new OreGeneration(), 1);

	}

	@EventHandler
	public void Init(FMLInitializationEvent event) {
		comProxy.registerRenders();
	}

	@EventHandler
	public void postInit(FMLPostInitializationEvent event) {
	}
}
