package fr.yslohr.trinodium.items;

import fr.yslohr.trinodium.References;
import fr.yslohr.trinodium.Trinodium;
import net.minecraft.item.Item;

public class ItemIngotTrinodium extends Item {
	public ItemIngotTrinodium() {
		this.setCreativeTab(Trinodium.tabTrinodium);
		this.setTextureName(References.MOD_ID + ":trinodium_ingot");
		this.setUnlocalizedName("trinodium_ingot");
	}
}
