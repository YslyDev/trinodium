package fr.yslohr.trinodium.items.tools;

import fr.yslohr.trinodium.References;
import fr.yslohr.trinodium.Trinodium;
import fr.yslohr.trinodium.init.ItemsRegistry;
import net.minecraft.item.ItemSpade;
import net.minecraft.item.ItemStack;

public class ItemShovelTrinodium extends ItemSpade {

	public ItemShovelTrinodium(ToolMaterial trinodiumToolMaterial) {
		super(trinodiumToolMaterial);
		this.setCreativeTab(Trinodium.tabTrinodium);
		this.setUnlocalizedName("trinodium_shovel");
		this.setTextureName(References.MOD_ID + ":trinodium_shovel");
	}
	@Override
	public boolean getIsRepairable(ItemStack input, ItemStack repair) {

		if (repair.getItem() == ItemsRegistry.trinodium_ingot) {
			return true;
		}
		return false;
	}
}
