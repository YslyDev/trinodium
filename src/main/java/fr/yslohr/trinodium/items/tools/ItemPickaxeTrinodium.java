package fr.yslohr.trinodium.items.tools;

import fr.yslohr.trinodium.References;
import fr.yslohr.trinodium.Trinodium;
import fr.yslohr.trinodium.init.ItemsRegistry;
import net.minecraft.item.ItemPickaxe;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Item.ToolMaterial;

public class ItemPickaxeTrinodium extends ItemPickaxe {

	public ItemPickaxeTrinodium(ToolMaterial trinodiumToolMaterial) {
		super(trinodiumToolMaterial);
		this.setCreativeTab(Trinodium.tabTrinodium);
		this.setTextureName(References.MOD_ID + ":trinodium_pickaxe");
		this.setUnlocalizedName("trinodium_pickaxe");
	}

	@Override
	public boolean getIsRepairable(ItemStack input, ItemStack repair) {
		if (repair.getItem() == ItemsRegistry.trinodium_ingot) {
			return true;
		}
		return false;
	}
}
