package fr.yslohr.trinodium.items.tools;

import fr.yslohr.trinodium.References;
import fr.yslohr.trinodium.Trinodium;
import fr.yslohr.trinodium.init.ItemsRegistry;
import net.minecraft.item.ItemAxe;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Item.ToolMaterial;

public class ItemAxeTrinodium extends ItemAxe {
	public ItemAxeTrinodium(ToolMaterial trinodiumToolMaterial) {
		super(trinodiumToolMaterial);
		this.setCreativeTab(Trinodium.tabTrinodium);
		this.setUnlocalizedName("trinodium_axe");
		this.setTextureName(References.MOD_ID + ":trinodium_axe");
	}

	@Override
	public boolean getIsRepairable(ItemStack input, ItemStack repair) {
		if (repair.getItem() == ItemsRegistry.trinodium_ingot) {
			return true;
		}
		return false;
	}
}
