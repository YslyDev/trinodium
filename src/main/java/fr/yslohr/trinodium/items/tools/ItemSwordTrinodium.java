package fr.yslohr.trinodium.items.tools;

import fr.yslohr.trinodium.References;
import fr.yslohr.trinodium.Trinodium;
import fr.yslohr.trinodium.init.ItemsRegistry;
import net.minecraft.item.ItemStack;
import net.minecraft.item.ItemSword;

public class ItemSwordTrinodium extends ItemSword {

	public ItemSwordTrinodium(ToolMaterial trinodiumToolMaterial) {
		super(trinodiumToolMaterial);
		this.setCreativeTab(Trinodium.tabTrinodium);
		this.setUnlocalizedName("trinodium_sword");
		this.setTextureName(References.MOD_ID + ":trinodium_sword");
	}

	@Override
	public boolean getIsRepairable(ItemStack input, ItemStack repair) {
		if (repair.getItem() == ItemsRegistry.trinodium_ingot) {
			return true;
		}
		return false;
	}
}
