package fr.yslohr.trinodium.items;

import fr.yslohr.trinodium.References;
import fr.yslohr.trinodium.Trinodium;
import net.minecraft.item.Item;

public class ItemStickTrinodium extends Item {
	public ItemStickTrinodium() {
		this.setCreativeTab(Trinodium.tabTrinodium);
		this.setTextureName(References.MOD_ID + ":trinodium_stick");
		this.setUnlocalizedName("trinodium_stick");
		this.setFull3D();
	}
}
