package fr.yslohr.trinodium.items.armors;

import fr.yslohr.trinodium.References;
import fr.yslohr.trinodium.init.ItemsRegistry;
import net.minecraft.entity.Entity;
import net.minecraft.item.ItemArmor;
import net.minecraft.item.ItemArmor.ArmorMaterial;
import net.minecraft.item.ItemStack;

public class ItemArmorTrinodium extends ItemArmor {
	public ItemArmorTrinodium(ArmorMaterial trinodiumArmorMaterial, int metaData) {
		super(trinodiumArmorMaterial, 0, metaData);
	}

	public String getArmorTexture(ItemStack stack, Entity entity, int slot, String type) {
		if (stack.getItem() == ItemsRegistry.trinodium_leggings) {
			return References.MOD_ID + ":textures/models/armor/trinodium_layer_2.png";
		} else if (stack.getItem() == ItemsRegistry.trinodium_helmet || stack.getItem() == ItemsRegistry.trinodium_chestplate
				|| stack.getItem() == ItemsRegistry.trinodium_boots) {
			return References.MOD_ID + ":textures/models/armor/trinodium_layer_1.png";
		}
		return null;
	}

	public boolean getIsRepairable(ItemStack input, ItemStack repair) {
		if (repair.getItem() == ItemsRegistry.trinodium_ingot) {
			return true;
		}
		return false;
	}
}
