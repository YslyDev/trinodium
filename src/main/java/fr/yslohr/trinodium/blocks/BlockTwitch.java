package fr.yslohr.trinodium.blocks;

import fr.yslohr.trinodium.References;
import fr.yslohr.trinodium.Trinodium;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;

public class BlockTwitch extends Block {

	public BlockTwitch(Material twitch) {
		super(twitch);
		this.setBlockName("twitch_block");
		this.setBlockTextureName(References.MOD_ID + ":twitch_block");
		this.setCreativeTab(Trinodium.tabTrinodium);
		this.setHardness(5.0F);
		this.setResistance(10.0F);
		this.setStepSound(soundTypeMetal);
	}
}
