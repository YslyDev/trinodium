package fr.yslohr.trinodium.blocks;

import fr.yslohr.trinodium.References;
import fr.yslohr.trinodium.Trinodium;
import net.minecraft.block.BlockOre;
import net.minecraft.block.material.Material;

public class OreTrinodium extends BlockOre {
	public OreTrinodium(Material rock) {
		this.setBlockName("trinodium_ore");
		this.setBlockTextureName(References.MOD_ID +":trinodium_ore");
		this.setCreativeTab(Trinodium.tabTrinodium);
		this.setHardness(3.0F);
		this.setResistance(5.0F);
		this.setStepSound(soundTypePiston);
	}
}
