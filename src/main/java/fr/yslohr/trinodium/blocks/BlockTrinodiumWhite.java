package fr.yslohr.trinodium.blocks;

import fr.yslohr.trinodium.References;
import fr.yslohr.trinodium.Trinodium;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.world.IBlockAccess;

public class BlockTrinodiumWhite extends BlockTrinodium {

	public BlockTrinodiumWhite(Material trinodium_white) {
		super(trinodium_white);
		this.setBlockName("trinodium_white_block");
		this.setBlockTextureName(References.MOD_ID +":trinodium_white_block");
		this.setCreativeTab(Trinodium.tabTrinodium);
		this.setHardness(5.0F);
		this.setResistance(10.0F);
		this.setStepSound(soundTypeMetal);
	}

	@Override
	public boolean isBeaconBase(IBlockAccess worldObj, int x, int y, int z, int beaconX, int beaconY, int beaconZ) {
		return super.isBeaconBase(worldObj, x, y, z, beaconX, beaconY, beaconZ);
	}
}
