package fr.yslohr.trinodium.init;

import cpw.mods.fml.common.registry.GameRegistry;
import fr.yslohr.trinodium.blocks.BlockTrinodium;
import fr.yslohr.trinodium.blocks.BlockTrinodiumPurple;
import fr.yslohr.trinodium.blocks.BlockTrinodiumWhite;
import fr.yslohr.trinodium.blocks.BlockTwitch;
import fr.yslohr.trinodium.blocks.OreTrinodium;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;

public class BlocksRegistry {

	public static Block trinodium_ore;
	public static Block trinodium_block;
	public static Block trinodium_white_block;
	public static Block trinodium_purple_block;
	public static Block twitch_block;

	public static void init() {
		trinodium_block = new BlockTrinodium(Material.iron);
		trinodium_white_block = new BlockTrinodiumWhite(Material.iron);
		trinodium_purple_block = new BlockTrinodiumPurple(Material.iron);
		trinodium_ore = new OreTrinodium(Material.rock);
		twitch_block = new BlockTwitch(Material.iron);
	}

	public static void register() {
		registerBlocks(trinodium_block);
		registerBlocks(trinodium_purple_block);
		registerBlocks(trinodium_white_block);
		registerBlocks(trinodium_ore);
		registerBlocks(twitch_block);
	}
	
	protected static void registerBlocks(Block block) {
		GameRegistry.registerBlock(block, block.getUnlocalizedName().substring(5));
	}
}
