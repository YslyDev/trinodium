package fr.yslohr.trinodium.init;

import cpw.mods.fml.common.registry.GameRegistry;
import fr.yslohr.trinodium.References;
import fr.yslohr.trinodium.Trinodium;
import fr.yslohr.trinodium.items.ItemIngotTrinodium;
import fr.yslohr.trinodium.items.ItemStickTrinodium;
import fr.yslohr.trinodium.items.armors.ItemArmorTrinodium;
import fr.yslohr.trinodium.items.tools.ItemAxeTrinodium;
import fr.yslohr.trinodium.items.tools.ItemPickaxeTrinodium;
import fr.yslohr.trinodium.items.tools.ItemShovelTrinodium;
import fr.yslohr.trinodium.items.tools.ItemSwordTrinodium;
import net.minecraft.item.Item;
import net.minecraft.item.Item.ToolMaterial;
import net.minecraft.item.ItemArmor.ArmorMaterial;
import net.minecraftforge.common.util.EnumHelper;

public class ItemsRegistry {
	public static ToolMaterial trinodiumToolMaterial = EnumHelper.addToolMaterial("trinodiumToolMaterial", 3, 1500,
			24.0F, 11.0F, 20);
	public static ArmorMaterial trinodiumArmorMaterial = EnumHelper.addArmorMaterial("trinodiumArmorMaterial", 94,
			new int[] { 4, 9, 7, 4 }, 20);

	public static Item trinodium_ingot;
	public static Item trinodium_stick;

	public static Item trinodium_sword;
	public static Item trinodium_shovel;
	public static Item trinodium_pickaxe;
	public static Item trinodium_axe;

	public static Item trinodium_helmet;
	public static Item trinodium_chestplate;
	public static Item trinodium_leggings;
	public static Item trinodium_boots;

	public static void init() {
		trinodium_ingot = new ItemIngotTrinodium();
		trinodium_stick = new ItemStickTrinodium();

		trinodium_sword = new ItemSwordTrinodium(trinodiumToolMaterial);
		trinodium_shovel = new ItemShovelTrinodium(trinodiumToolMaterial);
		trinodium_pickaxe = new ItemPickaxeTrinodium(trinodiumToolMaterial);
		trinodium_axe = new ItemAxeTrinodium(trinodiumToolMaterial);

		trinodium_helmet = new ItemArmorTrinodium(trinodiumArmorMaterial, 0).setCreativeTab(Trinodium.tabTrinodium)
				.setUnlocalizedName("trinodium_helmet").setTextureName(References.MOD_ID + ":trinodium_helmet");
		trinodium_chestplate = new ItemArmorTrinodium(trinodiumArmorMaterial, 1).setCreativeTab(Trinodium.tabTrinodium)
				.setUnlocalizedName("trinodium_chestplate").setTextureName(References.MOD_ID + ":trinodium_chestplate");
		trinodium_leggings = new ItemArmorTrinodium(trinodiumArmorMaterial, 2).setCreativeTab(Trinodium.tabTrinodium)
				.setUnlocalizedName("trinodium_leggings").setTextureName(References.MOD_ID + ":trinodium_leggings");
		trinodium_boots = new ItemArmorTrinodium(trinodiumArmorMaterial, 3).setCreativeTab(Trinodium.tabTrinodium)
				.setUnlocalizedName("trinodium_boots").setTextureName(References.MOD_ID + ":trinodium_boots");
	}

	public static void register() {
		itemsRegister(trinodium_ingot);
		itemsRegister(trinodium_stick);

		itemsRegister(trinodium_sword);
		itemsRegister(trinodium_shovel);
		itemsRegister(trinodium_pickaxe);
		itemsRegister(trinodium_axe);

		itemsRegister(trinodium_helmet);
		itemsRegister(trinodium_chestplate);
		itemsRegister(trinodium_leggings);
		itemsRegister(trinodium_boots);
	}

	protected static void itemsRegister(Item item) {
		GameRegistry.registerItem(item, item.getUnlocalizedName().substring(5));
	}
}
