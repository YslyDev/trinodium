package fr.yslohr.trinodium.init;

import cpw.mods.fml.common.registry.GameRegistry;
import net.minecraft.init.Items;
import net.minecraft.item.ItemStack;
import net.minecraftforge.oredict.OreDictionary;
import net.minecraftforge.oredict.ShapedOreRecipe;
import net.minecraftforge.oredict.ShapelessOreRecipe;

public class OreDictionaryRegistry {

	public static void register() {
		OreDictionary.registerOre("ingotTrinodium", ItemsRegistry.trinodium_ingot);
		OreDictionary.registerOre("stickTrinodium", ItemsRegistry.trinodium_stick);
		OreDictionary.registerOre("stickWood", Items.stick);
		OreDictionary.registerOre("blockTrinodium", BlocksRegistry.trinodium_block);
		OreDictionary.registerOre("blockTrinodiumWhite", BlocksRegistry.trinodium_white_block);
		OreDictionary.registerOre("blockTrinodiumPurple", BlocksRegistry.trinodium_purple_block);
	}

	public static void crafting() {
		GameRegistry.addRecipe(new ShapelessOreRecipe(new ItemStack(BlocksRegistry.trinodium_block), "ingotTrinodium",
				"ingotTrinodium", "ingotTrinodium", "ingotTrinodium", "ingotTrinodium", "ingotTrinodium",
				"ingotTrinodium", "ingotTrinodium", "ingotTrinodium"));
		GameRegistry
				.addRecipe(new ShapelessOreRecipe(new ItemStack(ItemsRegistry.trinodium_ingot, 9), "blockTrinodium"));
		GameRegistry.addSmelting(BlocksRegistry.trinodium_ore, new ItemStack(ItemsRegistry.trinodium_ingot), 1.0F);

		//Items
		GameRegistry.addRecipe(new ShapedOreRecipe(ItemsRegistry.trinodium_stick, " T ", " B ", 'T', "ingotTrinodium", 'B',"stickWood"));
		
		// Tools
		GameRegistry.addRecipe(new ShapedOreRecipe(ItemsRegistry.trinodium_sword, "T", "T", "B", 'T', "ingotTrinodium",
				'B', "stickTrinodium"));
		GameRegistry.addRecipe(new ShapedOreRecipe(ItemsRegistry.trinodium_shovel, "T", "B", "B", 'P', "ingotTrinodium",
				'B', "stickTrinodium"));
		GameRegistry.addRecipe(new ShapedOreRecipe(ItemsRegistry.trinodium_pickaxe, "TTT", " B ", " B ", 'T',
				"ingotTrinodium", 'B', "stickTrinodium"));
		GameRegistry.addRecipe(new ShapedOreRecipe(ItemsRegistry.trinodium_axe, " TT", " BT", " B ", 'T',
				"ingotTrinodium", 'B', "stickTrinodium"));

		// Armors
		GameRegistry
				.addRecipe(new ShapedOreRecipe(ItemsRegistry.trinodium_helmet, "TTT", "T T", 'T', "ingotTrinodium"));
		GameRegistry.addRecipe(
				new ShapedOreRecipe(ItemsRegistry.trinodium_chestplate, "T T", "TTT", "TTT", 'T', "ingotTrinodium"));
		GameRegistry.addRecipe(
				new ShapedOreRecipe(ItemsRegistry.trinodium_leggings, "TTT", "T T", "T T", 'T', "ingotTrinodium"));
		GameRegistry.addRecipe(new ShapedOreRecipe(ItemsRegistry.trinodium_boots, "T T", "T T", 'T', "ingotTrinodium"));
	}
}
